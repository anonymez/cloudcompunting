FROM python:3.6.6-jessie
WORKDIR /usr/src
COPY ./ .
RUN ["pip","install","-r","requirements.txt"]
CMD ["python","start.py"]