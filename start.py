import os
from flask import Flask,request
from flask_mysqldb import MySQL
import namesgenerator
app = Flask(__name__)


app.config['MYSQL_HOST'] = '127.0.0.1'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'my-secret-pw'
app.config['MYSQL_DB'] = 'names'
app.config['MYSQL_UNIX_SOCKET']='TCP'




mysql = MySQL(app)

@app.route('/',methods=['GET', 'POST'])
def hello_world():
  if request.method == "POST":
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO random(mkey,mvalue )  VALUES (%s, %s)", (namesgenerator.get_random_name(), namesgenerator.get_random_name()))
        mysql.connection.commit()
        cur.close()
        return "New Random key,value"
  else:
    cur = mysql.connection.cursor()
    cur.execute("SELECT mkey, mvalue FROM random LIMIT 50")
    result = cur.fetchall()
    print "result"
    print result
    return str(result)
    #return "Hello, World!"
 
if __name__ == "__main__":
  port = int(os.environ.get("PORT", 5000))
  host = os.environ.get("HOST", "0.0.0.0")
  app.run(host=host, port=port)

